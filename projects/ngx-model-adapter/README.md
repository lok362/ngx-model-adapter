# ngx-model-adapter

Model adapter service for Angular utilizing [class-validator](https://github.com/typestack/class-validator) and [class-transformer](https://github.com/typestack/class-transformer)

Note: Only tested in 7.x, but should work with 6.x and later.

# Installation

```sh
npm i -S ngx-model-adapter class-transformer class-validator
```

# Usage
```typescript
import { HttpClient } from "@angular/common/http";
import { ModelAdapter } from "ngx-model-adapter";
import { IsString, IsNumber } from "class-validator";
import { Expose, Transform } from "class-transformer";

// Define a model
export class Model {

  @IsString()
  @Expose({ name: "modelStr" })
  public str: string;

  @IsNumber()
  @Transform(v => Number.parseInt(v, 10), { toClassOnly: true })
  @Transform(v => String(v), { toPlainOnly: true })
  public num: number;

}

@Component(...)
export class AppComponent {

  constructor(
    private readonly http: HttpClient,
    private readonly modelAdapter: ModelAdapter
  ) {}
  
  // Get data from server and map to model
  public getModel() {
    return this.http.get(url).pipe(
      modelAdapter.mapToModel(Model)
    ).subscribe(
      model => model instanceof Model, // === true
      err => err // === ValidationError[] | HttpErrorResponse
    );
  }

  // Map model to plain object and post to server
  public postModel() {
    return modelAdapter.mapFromModel(model).pipe(
      switchMap(plain => httpClient.post(url, plain))
    ).subscribe(
      () => {}, // 200 OK
      err => err // === ValidationError[] | HttpErrorResponse
    );
  }

}

```

# Development

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test ngx-model-adapter` to execute the unit tests via [Karma](https://karma-runner.github.io).
