import { Injectable, InjectionToken, Inject } from "@angular/core";
import { OperatorFunction, from, throwError, Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { validate, ValidatorOptions } from "class-validator";
import { plainToClass, classToPlain, ClassTransformOptions } from "class-transformer";

export interface ModelAdapterOptions {
  validator?: ValidatorOptions;
  transformer?: ClassTransformOptions;
}

/**
 * Model Adapter Options
 *
 * Usage:
 * ```ts
 *  @NgModule({
 *    providers: [
 *      {
 *        provide: MODEL_ADAPTER_OPTIONS,
 *        useValue: {
 *          validator: {}, // ValidatorOptions (class-validator)
 *          transformer: {} // ClassTransformOptions (class-transformer)
 *        }
 *      }
 *    ]
 *  })
 *  export class AppModule {}
 * ```
 */
export const MODEL_ADAPTER_OPTIONS = new InjectionToken<ModelAdapterOptions>("ModelAdapterValidatorOptions", {
  providedIn: "root",
  factory: () => ({})
});

// @dynamic
@Injectable({ providedIn: "root" })
export class ModelAdapterService {

  constructor(@Inject(MODEL_ADAPTER_OPTIONS) private readonly options: ModelAdapterOptions) {}

  /**
   * Map a plain object to a model and validate it
   *
   * Usage:
   * ```ts
   *  httpClient.get(url).pipe(modelAdapter.mapToModel(Model)).subscribe(
   *    model => model instanceof Model, // === true
   *    errors => errors // === ValidationError[]
   *  );
   * ```
   */
  public mapToModel<T>(
    modelType: new () => T,
    optionsOverride?: ModelAdapterOptions
  ): OperatorFunction<unknown, T> {
    const opts = this.overrideOptions(optionsOverride);
    return switchMap(value => {
      if (typeof value !== "object") return throwError(new Error("value is not an object"));
      const model = plainToClass(modelType, value, opts.transformer);
      return from(validate(model, opts.validator)).pipe(map(errors => {
        if (errors.length > 0) throw errors;
        return model;
      }));
    });
  }

  /**
   * Validate a model and map it to a plain object
   *
   * Usage:
   * ```ts
   *  modelAdapter.mapFromModel(model).pipe(switchMap(plain => httpClient.post(url, plain))).subscribe(...);
   * ```
   */
  public mapFromModel<T>(
    model: T,
    optionsOverride?: ModelAdapterOptions
  ): Observable<any> {
    const opts = this.overrideOptions(optionsOverride);
    return from(validate(model, opts.validator)).pipe(map(errors => {
      if (errors.length > 0) throw errors;
      return classToPlain(model, opts.transformer) as any;
    }));
  }

  /** Helper method for overriding global options per method */
  private overrideOptions(options?: ModelAdapterOptions): ModelAdapterOptions {
    return options ? {
      validator: options.validator ? { ...this.options.validator, ...options.validator } : this.options.validator,
      transformer: options.transformer ? { ...this.options.transformer, ...options.transformer } : this.options.transformer
    } : this.options;
  }

}
