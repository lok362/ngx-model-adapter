import { TestBed, async } from "@angular/core/testing";

import { ModelAdapterService } from "./model-adapter.service";
import { IsString, IsNumber } from "class-validator";
import { of } from "rxjs";
import { Expose, Transform } from "class-transformer";

describe("ModelAdapterService", () => {
  let service: ModelAdapterService;
  class Model { @IsString() str: string; }
  class ModelExt {
    @IsString()
    @Expose({ name: "modelExtStr" })
    public str: string;
    @IsNumber()
    @Transform(v => Number.parseInt(v, 10), { toClassOnly: true })
    @Transform(v => String(v), { toPlainOnly: true })
    public num: number;
  }

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ModelAdapterService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  describe("#mapToModel", () => {

    it("instantiates model class from plain object", async(() => {
      const plain = { str: "banana" };
      of(plain).pipe(service.mapToModel(Model)).subscribe(res => {
        expect(res instanceof Model).toBeTruthy();
        expect(res.str).toBe("banana");
      });
    }));

    it("throws error if validation fails", async(() => {
      const plain = { str: 123 };
      of(plain).pipe(service.mapToModel(Model)).subscribe(
        () => expect(true).toBeFalsy(),
        errors => expect(errors).toBeTruthy()
      );
    }));

    it("maps transformed and exposed properties correctly", async(() => {
      const plain = {
        modelExtStr: "banana",
        num: "123"
      };
      of(plain).pipe(service.mapToModel(ModelExt)).subscribe(res => {
        expect(res instanceof ModelExt).toBeTruthy();
        expect(typeof res.num === "number").toBeTruthy();
        expect(res.str === "banana").toBeTruthy();
      });
    }));

  });

  describe("#mapFromModel", () => {

    it("transforms model class to plain object", async(() => {
      const model = new Model();
      model.str = "banana";
      service.mapFromModel(model).subscribe(res => {
        expect(typeof res).toBe("object");
        expect(res.str).toBe("banana");
      });
    }));

    it("throws error if validation fails", async(() => {
      const model = new Model();
      service.mapFromModel(model).subscribe(
        () => expect(true).toBeFalsy(),
        errors => expect(errors).toBeTruthy()
      );
    }));

    it("maps transformed and exposed properties correctly", async(() => {
      const model = new ModelExt();
      model.str = "banana";
      model.num = 123;
      service.mapFromModel(model).subscribe(res => {
        expect(res).toEqual({
          modelExtStr: "banana",
          num: "123"
        });
      });
    }));

  });

});
